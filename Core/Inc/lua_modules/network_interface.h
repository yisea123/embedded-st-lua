#ifndef LUA_MODULES_NETWORK_INTERFACE_H
#define LUA_MODULES_NETWORK_INTERFACE_H

#include "lua_modules/common.h"

#define LUA_MODULES_NETWORK_INTERFACE_LIB_NAME "network_interface"

int luaopen_network_interface(lua_State *L);

#endif /* LUA_MODULES_NETWORK_INTERFACE_H */