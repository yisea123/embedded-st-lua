#ifndef LUA_MODULES_LCD_H
#define LUA_MODULES_LCD_H

#include "lua_modules/common.h"

#define LUA_MODULES_LCD_LIB_NAME "lcd"

int luaopen_lcd(lua_State *L);

#endif /* LUA_MODULES_LCD_H */