#include "lua_modules/mqtt_client.h"

#include <string.h>

#include "FreeRTOS.h"

#include "rotable.h"

#include "lwip/ip_addr.h"
#include "lwip/apps/mqtt.h"
#include "lstate.h"

#define CLIENT_LUA_TYPE ("MqttClient")

static const struct luaL_Reg mqtt_client_class[];

struct callback_data_t
{
    lua_State *luaState;
    int threadRef;
    int connectionStatusHandlerRef;
    int subscribeHandlerRef;
    int currentTopicRef;
};

static int new_client(lua_State *L)
{
    mqtt_client_t *client = (mqtt_client_t *)lua_newuserdata(L, sizeof(mqtt_client_t));
    memset(client, 0, sizeof(mqtt_client_t));

    luaL_setmetatable(L, CLIENT_LUA_TYPE);

    return 1;
}

static int is_connected(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 1);
    mqtt_client_t *client = luaL_checkudata(L, 1, CLIENT_LUA_TYPE);

    lua_pushboolean(L, mqtt_client_is_connected(client));

    return 1;
}

static void publish_callback(void *arg, const char *topic, u32_t tot_len)
{
    struct callback_data_t *cd = (struct callback_data_t *)arg;

    if (cd->luaState == NULL || cd->subscribeHandlerRef == LUA_REFNIL)
        return;

    lua_State *L = cd->luaState;

    lua_pushstring(L, topic);

    cd->currentTopicRef = luaL_ref(L, LUA_REGISTRYINDEX);
}

static void data_callback(void *arg, const u8_t *data, u16_t len, u8_t flags)
{
    struct callback_data_t *cd = (struct callback_data_t *)arg;

    if (cd->luaState == NULL || cd->subscribeHandlerRef == LUA_REFNIL || cd->currentTopicRef == LUA_REFNIL)
        return;

    lua_State *L = cd->luaState;

    lua_rawgeti(L, LUA_REGISTRYINDEX, cd->subscribeHandlerRef); // f(...)

    lua_rawgeti(L, LUA_REGISTRYINDEX, cd->currentTopicRef); // #1
    lua_pushlstring(L, data, len); // #2
    lua_pushboolean(L, flags & MQTT_DATA_FLAG_LAST); // #3

    if(lua_pcall(L, 3, 0, 0) != 0)
    {
        printf("[ERROR] %s\n", lua_tostring(L, -1));
    }

    if (flags & MQTT_DATA_FLAG_LAST)
    {
        lua_pushnil(L);
        lua_rawseti(L, LUA_REGISTRYINDEX, cd->currentTopicRef);
        cd->currentTopicRef = LUA_REFNIL;
    }
}

static void connection_callback(mqtt_client_t *client, void *arg, mqtt_connection_status_t status)
{
    if (status == MQTT_CONNECT_ACCEPTED)
    {
        mqtt_set_inpub_callback(client, publish_callback, data_callback, arg);
    }

    struct callback_data_t *cd = (struct callback_data_t *)arg;
    if (cd->luaState == NULL || cd->connectionStatusHandlerRef == LUA_REFNIL || cd->currentTopicRef == LUA_REFNIL)
        return;

    lua_State *L = cd->luaState;

    lua_rawgeti(L, LUA_REGISTRYINDEX, cd->connectionStatusHandlerRef); // f(...)

    lua_pushinteger(L, status); // #1

    if(lua_pcall(L, 1, 0, 0) != 0)
    {
        printf("[ERROR] %s\n", lua_tostring(L, -1));
    }
}

static void cleanup_data(lua_State * L, struct mqtt_client_t *client)
{
    if (client->connect_arg)
    {
        struct callback_data_t * cd = client->connect_arg;

        // lua_close(cd->luaState); // done by GC

        lua_pushnil(L);
        lua_rawseti(L, LUA_REGISTRYINDEX, cd->threadRef);

        if (cd->connectionStatusHandlerRef != LUA_REFNIL)
        {
            lua_pushnil(L);
            lua_rawseti(L, LUA_REGISTRYINDEX, cd->connectionStatusHandlerRef);
        }

        if (cd->subscribeHandlerRef != LUA_REFNIL)
        {
            lua_pushnil(L);
            lua_rawseti(L, LUA_REGISTRYINDEX, cd->subscribeHandlerRef);
        }

        if (cd->currentTopicRef != LUA_REFNIL)
        {
            lua_pushnil(L);
            lua_rawseti(L, LUA_REGISTRYINDEX, cd->currentTopicRef);
        }

        free(client->connect_arg);
        client->connect_arg = NULL;
    }
}

static int connect(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 5);
    struct mqtt_client_t *client = luaL_checkudata(L, 1, CLIENT_LUA_TYPE);

    const char *ip_string = luaL_checkstring(L, 2);
    const int port = luaL_checkinteger(L, 3);
    luaL_checktype(L, 4, LUA_TTABLE);
    luaL_checktype(L, 5, LUA_TFUNCTION);

    struct ip4_addr ip;
    struct mqtt_connect_client_info_t client_info;
    memset(&client_info, 0, sizeof(client_info));

    if (!ip4addr_aton(ip_string, &ip))
    {
        return 0;
    }

    {
        const int id_type = lua_getfield(L, 4, "id");

        if (id_type != LUA_TSTRING)
        {
            return luaL_error(L, "`id' field in client info is required and has to be string");
        }

        client_info.client_id = lua_tostring(L, -1);
        lua_pop(L, 1);
    }

    {
        const int user_type = lua_getfield(L, 4, "user");

        if (user_type == LUA_TSTRING)
        {
            client_info.client_user = lua_tostring(L, -1);
        }
        else if (user_type != LUA_TNIL)
        {
            return luaL_error(L, "`user' field in client info has to be string");
        }
        lua_pop(L, 1);
    }

    {
        const int pass_type = lua_getfield(L, 4, "pass");

        if (pass_type == LUA_TSTRING)
        {
            client_info.client_pass = lua_tostring(L, -1);
        }
        else if (pass_type != LUA_TNIL)
        {
            return luaL_error(L, "`pass' field in client info has to be string");
        }
        lua_pop(L, 1);
    }

    {
        const int keep_alive_type = lua_getfield(L, 4, "keep_alive");

        if (!lua_isinteger(L, -1))
        {
            client_info.keep_alive = lua_tointeger(L, -1);
        }
        else if (keep_alive_type != LUA_TNIL)
        {
            return luaL_error(L, "`keep_alive' field in client info has to be integer");
        }
        lua_pop(L, 1);
    }

    {
        const int will_topic_type = lua_getfield(L, 4, "will_topic");

        if (will_topic_type == LUA_TSTRING)
        {
            client_info.will_topic = lua_tostring(L, -1);
        }
        else if (will_topic_type != LUA_TNIL)
        {
            return luaL_error(L, "`will_topic' field in client info has to be string");
        }
        lua_pop(L, 1);
    }

    {
        const int will_message_type = lua_getfield(L, 4, "will_message");

        if (will_message_type == LUA_TSTRING)
        {
            client_info.will_msg = lua_tostring(L, -1);
        }
        else if (will_message_type != LUA_TNIL)
        {
            return luaL_error(L, "`will_message' field in client info has to be string");
        }
        lua_pop(L, 1);
    }

    {
        const int will_qos_type = lua_getfield(L, 4, "will_qos");

        if (!lua_isinteger(L, -1))
        {
            client_info.will_qos = lua_tointeger(L, -1);
        }
        else if (will_qos_type != LUA_TNIL)
        {
            return luaL_error(L, "`will_qos' field in client info has to be integer");
        }
        lua_pop(L, 1);
    }

    {
        const int will_retain_type = lua_getfield(L, 4, "will_retain");

        if (!lua_isinteger(L, -1))
        {
            client_info.will_retain = lua_tointeger(L, -1);
        }
        else if (will_retain_type != LUA_TNIL)
        {
            return luaL_error(L, "`will_retain' field in client info has to be integer");
        }
        lua_pop(L, 1);
    }

    cleanup_data(L, client);

    lua_State *NL = lua_newthread(L);
    int thread_ref = luaL_ref(L, LUA_REGISTRYINDEX);

    struct callback_data_t *callbackData = malloc(sizeof(struct callback_data_t));
    callbackData->luaState = NL;
    callbackData->threadRef = thread_ref;
    callbackData->subscribeHandlerRef = LUA_REFNIL;

    lua_pushvalue(L, 5);
    callbackData->connectionStatusHandlerRef = luaL_ref(L, LUA_REGISTRYINDEX);

    err_t result = mqtt_client_connect(client, &ip, port, connection_callback, callbackData, &client_info);

    if (result != ERR_OK)
    {
        cleanup_data(L, client);
    }

    lua_pushboolean(L, result == ERR_OK);

    return 1;
}

static int disconnect(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 1);
    mqtt_client_t *client = luaL_checkudata(L, 1, CLIENT_LUA_TYPE);

    if (mqtt_client_is_connected(client))
    {
        mqtt_disconnect(client);
    }

    cleanup_data(L, client);

    memset(client, 0, sizeof(mqtt_client_t));

    return 0;
}

static int subscribe(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 3);
    mqtt_client_t *client = luaL_checkudata(L, 1, CLIENT_LUA_TYPE);

    const char *topic = luaL_checkstring(L, 2);
    const int qos = luaL_checkinteger(L, 3);

    lua_pushboolean(L, mqtt_subscribe(client, topic, qos, NULL, NULL) == ERR_OK);

    return 1;
}

static int unsubscribe(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 2);
    mqtt_client_t *client = luaL_checkudata(L, 1, CLIENT_LUA_TYPE);

    const char *topic = luaL_checkstring(L, 2);

    lua_pushboolean(L, mqtt_unsubscribe(client, topic, NULL, NULL) == ERR_OK);

    return 1;
}

static int publish(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 5);
    mqtt_client_t *client = luaL_checkudata(L, 1, CLIENT_LUA_TYPE);

    const char *topic = luaL_checkstring(L, 2);
    size_t length = 0;
    const char *payload = luaL_checklstring(L, 3, &length);
    const int qos = luaL_checkinteger(L, 4);
    if (!lua_isboolean(L, 5))
    {
        return luaL_error(L, "5th argument has to be boolean");
    }

    const int retain = lua_toboolean(L, 5);

    lua_pushboolean(L, mqtt_publish(client, topic, payload, length, qos, retain, NULL, NULL) == ERR_OK);

    return 1;
}

static int setIncomingDataHandler(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 2);
    mqtt_client_t *client = luaL_checkudata(L, 1, CLIENT_LUA_TYPE);
    struct callback_data_t *cd = (struct callback_data_t *)client->connect_arg;
    if (cd->subscribeHandlerRef != LUA_REFNIL)
    {
        lua_pushnil(L);
        lua_rawseti(L, LUA_REGISTRYINDEX, cd->subscribeHandlerRef);
    }

    lua_pushvalue(L, 2);
    cd->subscribeHandlerRef = luaL_ref(L, LUA_REGISTRYINDEX);

    return 0;
}

static const luaL_Reg lib[] = {
    {"new", new_client},
    {NULL, NULL}};

static const struct luaL_Reg mqtt_client_class[] = {
    {"isConnected", is_connected},
    {"connect", connect},
    {"disconnect", disconnect},
    {"subscribe", subscribe},
    {"unsubscribe", unsubscribe},
    {"publish", publish},
    {"setIncomingDataHandler", setIncomingDataHandler},
    {NULL, NULL}};

int luaopen_mqtt_client(lua_State *L)
{
    luaL_newmetatable(L, CLIENT_LUA_TYPE);

    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");

    lua_pushcfunction(L, disconnect);
    lua_setfield(L, -2, "__gc");

    luaL_setfuncs(L, mqtt_client_class, 0);

    rotable_newlib(L, lib);
    return 1;
}