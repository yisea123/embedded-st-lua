#include "lua_modules/network_interface.h"

#include "rotable.h"

#include "netif.h"
#include "netifapi.h"

#include "lwip.h"

#define _NETIFAPI_IS_DHCP_UP(n) (netif_dhcp_data((n)) != NULL)

static int init(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    MX_LWIP_Init();
    return 0;
}

static int is_up(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    lua_pushboolean(L, netif_is_up(netif_default));
    return 1;
}

static int set_up(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    netif_set_up(netif_default);
    return 0;
}

static int set_down(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    netifapi_dhcp_stop(netif_default);
    netif_set_down(netif_default);
    return 0;
}

static int is_link_up(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    lua_pushboolean(L, netif_is_link_up(netif_default));
    return 1;
}

static int link_set_up(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    netif_set_up(netif_default);
    return 0;
}

static int link_set_down(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    netifapi_dhcp_stop(netif_default);
    netif_set_down(netif_default);
    return 0;
}

static int dhcp_client_enable(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    netifapi_dhcp_start(netif_default);
    return 0;
}

static int dhcp_client_disable(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    netifapi_dhcp_stop(netif_default);
    return 0;
}

static int dhcp_client_is_up(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    lua_pushboolean(L, _NETIFAPI_IS_DHCP_UP(netif_default));
    return 0;
}

#pragma region SETTERS

static int parse_ip4_from_string(lua_State *L, struct ip4_addr *ip)
{
    if (lua_type(L, -1) != LUA_TSTRING)
    {
        return 0;
    }

    const char *s = lua_tostring(L, -1);

    if (!ip4addr_aton(s, ip))
    {
        return 0;
    }

    return 1;
}

static int parse_ip4_from_ints(lua_State *L, struct ip4_addr *ip)
{
    if (lua_isinteger(L, -1) && lua_isinteger(L, -2) && lua_isinteger(L, -3) && lua_isinteger(L, -4))
    {
        const int a = lua_tointeger(L, -4);
        const int b = lua_tointeger(L, -3);
        const int c = lua_tointeger(L, -2);
        const int d = lua_tointeger(L, -1);

        if (a < 0 || a > 255 || b < 0 || b > 255 || c < 0 || c > 255 || d < 0 || d > 255)
        {
            return 0;
        }

        IP4_ADDR(ip, a, b, c, d);
        return 1;
    }
    else
    {
        return 0;
    }
}

static int parse_ip4_from_function_arguments(lua_State *L, struct ip4_addr *ip)
{
    if (lua_gettop(L) == 1)
    {
        return parse_ip4_from_string(L, ip);
    }
    else if (lua_gettop(L) == 4)
    {
        return parse_ip4_from_ints(L, ip);
    }
    else
    {
        luaL_error(L, "this function expects 1 or 4 arguments, got %d", lua_gettop(L));
    }

    return 0;
}

static int set_ip4(lua_State *L)
{
    struct ip4_addr ip;

    if (!parse_ip4_from_function_arguments(L, &ip))
    {
        return luaL_error(L, "invalid arguments. Expected IPv4");
    }

    netif_set_ipaddr(netif_default, &ip);

    return 0;
}

static int set_ip4_mask(lua_State *L)
{
    struct ip4_addr mask;

    if (!parse_ip4_from_function_arguments(L, &mask))
    {
        return luaL_error(L, "invalid arguments. Expected IPv4");
    }

    if (!ip4_addr_netmask_valid(mask.addr))
    {
        return luaL_error(L, "`mask' is not valid IPv4 mask");
    }

    netif_set_netmask(netif_default, &mask);

    return 0;
}

static int set_ip4_gateway(lua_State *L)
{
    struct ip4_addr gw;

    if (!parse_ip4_from_function_arguments(L, &gw))
    {
        return luaL_error(L, "invalid arguments. Expected IPv4");
    }

    netif_set_gw(netif_default, &gw);

    return 0;
}

// set_ip4_configuration({ip = "...", mask = "..."[, gateway = "..."]})
static int set_ip4_configuration(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 1);

    luaL_checktype(L, 1, LUA_TTABLE);

    const int ip_type = lua_getfield(L, 1, "ip");

    if (ip_type == LUA_TSTRING)
    {
        struct ip4_addr ip;
        if (!parse_ip4_from_string(L, &ip))
        {
            return luaL_error(L, "invalid type of `ip' field. Expected IPv4 address");
        }
        netif_set_ipaddr(netif_default, &ip);
    }
    else
    {
        return luaL_error(L, "invalid type of `ip' field. Expected string representation of IPv4 address");
    }

    lua_pop(L, 1);

    const int mask_type = lua_getfield(L, 1, "mask");

    if (mask_type == LUA_TSTRING)
    {
        struct ip4_addr mask;
        if (!parse_ip4_from_string(L, &mask))
        {
            return luaL_error(L, "invalid type of `mask' field. Expected IPv4 address");
        }
        if (!ip4_addr_netmask_valid(mask.addr))
        {
            return luaL_error(L, "`mask' is not valid IPv4 mask");
        }

        netif_set_netmask(netif_default, &mask);
    }
    else
    {
        return luaL_error(L, "invalid type of `mask' field. Expected string representation of IPv4 address");
    }

    lua_pop(L, 1);

    if (lua_getfield(L, 1, "gateway") == LUA_TSTRING)
    {
        struct ip4_addr gw;
        if (!parse_ip4_from_string(L, &gw))
        {
            return luaL_error(L, "invalid type of `gateway' field. Expected IPv4 address");
        }
        netif_set_gw(netif_default, &gw);
    }

    lua_pop(L, 1);

    return 0;
}

#pragma region GETTERS

static int get_ip4(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    lua_pushstring(L, ip4addr_ntoa(&(netif_default->ip_addr)));
    return 1;
}

static int get_ip4_mask(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    lua_pushstring(L, ip4addr_ntoa(&(netif_default->netmask)));
    return 0;
}

static int get_ip4_gateway(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    lua_pushstring(L, ip4addr_ntoa(&(netif_default->gw)));
    return 0;
}

static int get_ip4_configuration(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    
    lua_createtable(L, 0, 3);

    lua_pushstring(L, ip4addr_ntoa(&(netif_default->ip_addr)));
    lua_setfield(L, -2, "ip");

    lua_pushstring(L, ip4addr_ntoa(&(netif_default->netmask)));
    lua_setfield(L, -2, "mask");

    lua_pushstring(L, ip4addr_ntoa(&(netif_default->gw)));
    lua_setfield(L, -2, "gateway");

    return 1;
}

#pragma region API code

static const luaL_Reg lib[] = {
    {"init", init},
    {"is_up", is_up},
    {"set_up", set_up},
    {"set_down", set_down},
    {"is_link_up", is_link_up},
    {"link_set_up", link_set_up},
    {"link_set_down", link_set_down},
    {"dhcp_client_enable", dhcp_client_enable},
    {"dhcp_client_disable", dhcp_client_disable},
    {"dhcp_client_is_up", dhcp_client_is_up},
    {"set_ip4", set_ip4},
    {"set_ip4_mask", set_ip4_mask},
    {"set_ip4_gateway", set_ip4_gateway},
    {"set_ip4_configuration", set_ip4_configuration},
    {"get_ip4", get_ip4},
    {"get_ip4_mask", get_ip4_mask},
    {"get_ip4_gateway", get_ip4_gateway},
    {"get_ip4_configuration", get_ip4_configuration},

    {NULL, NULL}};

int luaopen_network_interface(lua_State *L)
{
    rotable_newlib(L, lib);
    return 1;
}
