#include "lua_modules/lcd.h"

#include "lua_modules/common.h"

#include "stm32746g_discovery_lcd.h"
#include "rotable.h"

#define LCD_X_SIZE RK043FN48H_WIDTH
#define LCD_Y_SIZE RK043FN48H_HEIGHT

static uint32_t lcd_layer_0[LCD_Y_SIZE][LCD_X_SIZE] __attribute__((section(".sdram"))) __attribute__((unused));
static uint32_t lcd_layer_1[LCD_Y_SIZE][LCD_X_SIZE] __attribute__((section(".sdram"))) __attribute__((unused));

static int init(lua_State *L)
{
    if (BSP_LCD_Init() != LCD_OK)
    {
        return luaL_error(L, "failed to initialize LCD");
    }

    BSP_LCD_LayerDefaultInit(0, (unsigned int)lcd_layer_0);
    BSP_LCD_LayerDefaultInit(1, (unsigned int)lcd_layer_1);

    return 0;
}

static int deinit(lua_State *L)
{
    BSP_LCD_DeInit();
    return 0;
}

static int set_display_on_off(lua_State *L)
{
    const int state = lua_toboolean(L, 1);

    if (state != 0)
    {
        BSP_LCD_DisplayOn();
    }
    else
    {
        BSP_LCD_DisplayOff();
    }

    return 0;
}

static int get_screen_size(lua_State *L)
{
    lua_createtable(L, 0, 2);

    lua_pushinteger(L, BSP_LCD_GetXSize());
    lua_setfield(L, -2, "x");

    lua_pushinteger(L, BSP_LCD_GetYSize());
    lua_setfield(L, -2, "y");

    return 1;
}

static int set_text_color(lua_State *L)
{
    uint32_t c = luaL_checkinteger(L, 1);
    //	Color_t * c = (Color_t *)lua_touserdata(L, 1);
    //
    //	luaL_argcheck(L, c != NULL, 1, "`Color' expected");
    //
    //	uint32_t color_value = ColorToBinaryValue(c);
    //
    //	BSP_LCD_SetTextColor(color_value);
    BSP_LCD_SetTextColor(c);

    return 0;
}

static int get_text_color(lua_State *L)
{
    lua_pushinteger(L, BSP_LCD_GetTextColor());

    return 1;
}

static int set_back_color(lua_State *L)
{
    uint32_t c = luaL_checkinteger(L, 1);

    BSP_LCD_SetBackColor(c);

    return 0;
}

static int get_back_color(lua_State *L)
{
    lua_pushinteger(L, BSP_LCD_GetBackColor());

    return 1;
}

static int read_pixel(lua_State *L)
{
    const lua_Integer x = luaL_checkinteger(L, 1);
    const lua_Integer y = luaL_checkinteger(L, 2);
    const uint32_t x_size = BSP_LCD_GetXSize();
    const uint32_t y_size = BSP_LCD_GetYSize();

    if (x >= 0 && (uint32_t) x < x_size && y >= 0 && (uint32_t) y < y_size)
    {
        lua_pushinteger(L, BSP_LCD_ReadPixel((uint16_t)x, (uint16_t)y));
        return 1;
    }
    else
    {
        return luaL_error(L, "position out of bounds for x: %d, y: %d, x_size: %ul, y_size: %ul", x, y, x_size, y_size);
    }
}

static int draw_pixel(lua_State *L)
{
    uint16_t x = luaL_checkinteger(L, 1);
    uint16_t y = luaL_checkinteger(L, 2);
    uint32_t c = luaL_checkinteger(L, 3);

    BSP_LCD_DrawPixel(x, y, c);

    return 0;
}

static int clear(lua_State *L)
{
    uint32_t c = luaL_checkinteger(L, 1);

    BSP_LCD_Clear(c);

    return 0;
}

static int clear_string_line(lua_State *L)
{
    uint32_t l = luaL_checkinteger(L, 1);

    BSP_LCD_ClearStringLine(l);

    return 0;
}

static int display_string(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 2);

    uint16_t line = luaL_checkinteger(L, 1);
    const char *s = luaL_checkstring(L, 2);
    BSP_LCD_DisplayStringAtLine(line, (uint8_t *)s);


    return 0;
}

static int display_string_at(lua_State *L)
{
    lua_modules_expect_exact_arguments_number(L, 4);

    uint16_t x = luaL_checkinteger(L, 1);
    uint16_t y = luaL_checkinteger(L, 2);
    const char *s = luaL_checkstring(L, 3);
    Text_AlignModeTypdef m = (Text_AlignModeTypdef)luaL_checkinteger(L, 4);

    BSP_LCD_DisplayStringAt(x, y, (uint8_t *)s, m);

    return 0;
}

static int display_char(lua_State *L)
{
    uint16_t x = luaL_checkinteger(L, 1);
    uint16_t y = luaL_checkinteger(L, 2);
    const char *s = luaL_checkstring(L, 3);

    BSP_LCD_DisplayChar(x, y, (uint8_t)s[0]);

    return 0;
}

static int draw_horizontal_line(lua_State *L)
{
    uint16_t x = luaL_checkinteger(L, 1);
    uint16_t y = luaL_checkinteger(L, 2);
    uint16_t l = luaL_checkinteger(L, 3);

    BSP_LCD_DrawHLine(x, y, l);

    return 0;
}

static int draw_vertical_line(lua_State *L)
{
    uint16_t x = luaL_checkinteger(L, 1);
    uint16_t y = luaL_checkinteger(L, 2);
    uint16_t l = luaL_checkinteger(L, 3);

    BSP_LCD_DrawVLine(x, y, l);

    return 0;
}

static int draw_line(lua_State *L)
{
    uint16_t x1 = luaL_checkinteger(L, 1);
    uint16_t y1 = luaL_checkinteger(L, 2);
    uint16_t x2 = luaL_checkinteger(L, 3);
    uint16_t y2 = luaL_checkinteger(L, 4);

    BSP_LCD_DrawLine(x1, y1, x2, y2);

    return 0;
}

static int draw_rect(lua_State *L)
{
    uint16_t x = luaL_checkinteger(L, 1);
    uint16_t y = luaL_checkinteger(L, 2);
    uint16_t w = luaL_checkinteger(L, 3);
    uint16_t h = luaL_checkinteger(L, 4);

    BSP_LCD_DrawRect(x, y, w, h);

    return 0;
}

static int draw_circle(lua_State *L)
{
    uint16_t x = luaL_checkinteger(L, 1);
    uint16_t y = luaL_checkinteger(L, 2);
    uint16_t r = luaL_checkinteger(L, 3);

    BSP_LCD_DrawCircle(x, y, r);

    return 0;
}

static int draw_ellipse(lua_State *L)
{
    uint16_t x = luaL_checkinteger(L, 1);
    uint16_t y = luaL_checkinteger(L, 2);
    uint16_t rx = luaL_checkinteger(L, 3);
    uint16_t ry = luaL_checkinteger(L, 4);

    BSP_LCD_DrawEllipse(x, y, rx, ry);

    return 0;
}

static int fill_rect(lua_State *L)
{
    uint16_t x = luaL_checkinteger(L, 1);
    uint16_t y = luaL_checkinteger(L, 2);
    uint16_t w = luaL_checkinteger(L, 3);
    uint16_t h = luaL_checkinteger(L, 4);

    BSP_LCD_FillRect(x, y, w, h);

    return 0;
}

static int fill_circle(lua_State *L)
{
    uint16_t x = luaL_checkinteger(L, 1);
    uint16_t y = luaL_checkinteger(L, 2);
    uint16_t r = luaL_checkinteger(L, 3);

    BSP_LCD_FillCircle(x, y, r);

    return 0;
}

static int fill_ellipse(lua_State *L)
{
    uint16_t x = luaL_checkinteger(L, 1);
    uint16_t y = luaL_checkinteger(L, 2);
    uint16_t rx = luaL_checkinteger(L, 3);
    uint16_t ry = luaL_checkinteger(L, 4);

    BSP_LCD_FillEllipse(x, y, rx, ry);

    return 0;
}

static void swap_buffers_raw()
{
    while (!(LTDC->CDSR & LTDC_CDSR_VSYNCS))
        ;

    BSP_LCD_SetLayerVisible(BSP_LCD_GetCurrentLayer(), ENABLE);
    uint32_t newLayerIndex = (BSP_LCD_GetCurrentLayer() + 1) % MAX_LAYER_NUMBER;

    BSP_LCD_SetLayerVisible(newLayerIndex, DISABLE);
    BSP_LCD_SelectLayer(newLayerIndex);
}

static int swap_buffers(lua_State *L)
{
    swap_buffers_raw();
    return 0;
}

static int make_color(lua_State *L)
{
    const uint32_t r = (uint32_t)(luaL_checknumber(L, 1) * 255);
    const uint32_t g = (uint32_t)(luaL_checknumber(L, 2) * 255);
    const uint32_t b = (uint32_t)(luaL_checknumber(L, 3) * 255);
    const uint32_t a = (uint32_t)(luaL_checknumber(L, 4) * 255);

    const uint32_t c = ((a << 24) || (r << 16) || (g << 8) || (b));

    lua_pushinteger(L, c);

    return 1;
}

static int set_font_size(lua_State * L)
{
    lua_modules_expect_exact_arguments_number(L, 1);

    const int size = luaL_checkinteger(L, 1);

    sFONT * font = NULL;

    switch(size)
    {
    case 8:
        font = &Font8;
        break;
    case 12:
        font = &Font12;
        break;
    case 16:
        font = &Font16;
        break;
    case 20:
        font = &Font20;
        break;
    case 24:
        font = &Font24;
        break;
    default:
        return luaL_error(L, "Unsupported font size. This implementation supports only forns of size {8, 12, 16, 20, 24}");
    }

    BSP_LCD_SetFont(font);
    swap_buffers_raw();
    BSP_LCD_SetFont(font);
    swap_buffers_raw();

    return 0;    
}

static int get_font_size(lua_State * L)
{
    lua_modules_expect_exact_arguments_number(L, 0);

    lua_pushinteger(L, BSP_LCD_GetFont()->Height);

    return 1;
}

static int get_font_width(lua_State * L)
{
    lua_modules_expect_exact_arguments_number(L, 0);

    lua_pushinteger(L, BSP_LCD_GetFont()->Width);

    return 1;
}

static const luaL_Reg lib[] = {
    /* Function defnies */
    {"init", init},
    {"deinit", deinit},

    {"set_display_on_off", set_display_on_off},

    {"get_screen_size", get_screen_size},

    {"set_text_color", set_text_color},
    {"get_text_color", get_text_color},
    {"set_back_color", set_back_color},
    {"get_back_color", get_back_color},

    {"read_pixel", read_pixel},
    {"draw_pixel", draw_pixel},
    {"clear", clear},
    {"clear_string_line", clear_string_line},
    {"display_string", display_string},
    {"display_string_at", display_string_at},
    {"display_char", display_char},

    {"draw_horizontal_line", draw_horizontal_line},
    {"draw_vertical_line", draw_vertical_line},
    {"draw_line", draw_line},
    {"draw_rect", draw_rect},
    {"draw_circle", draw_circle},
    //	{"draw_polygon", },
    {"draw_ellipse", draw_ellipse},
    //	{"draw_bitmap", draw_bitmap},
    {"fill_rect", fill_rect},
    {"fill_circle", fill_circle},
    //	{"fill_polygon", fill_polygon},
    {"fill_ellipse", fill_ellipse},

    {"swap_buffers", swap_buffers},

    {"make_color", make_color},

    {"set_font_size", set_font_size},

    {"get_font_size", get_font_size},
    {"get_font_width", get_font_width},

    /* Placeholders for colors */
    {"COLOR_BLUE", NULL},
    {"COLOR_GREEN", NULL},
    {"COLOR_RED", NULL},
    {"COLOR_CYAN", NULL},
    {"COLOR_MAGENTA", NULL},
    {"COLOR_YELLOW", NULL},
    {"COLOR_LIGHTBLUE", NULL},
    {"COLOR_LIGHTGREEN", NULL},
    {"COLOR_LIGHTRED", NULL},
    {"COLOR_LIGHTCYAN", NULL},
    {"COLOR_LIGHTMAGENTA", NULL},
    {"COLOR_DARKBLUE", NULL},
    {"COLOR_DARKGREEN", NULL},
    {"COLOR_DARKRED", NULL},
    {"COLOR_DARKCYAN", NULL},
    {"COLOR_DARKMAGENTA", NULL},
    {"COLOR_DARKYELLOW", NULL},
    {"COLOR_WHITE", NULL},
    {"COLOR_LIGHTGRAY", NULL},
    {"COLOR_GRAY", NULL},
    {"COLOR_DARKGRAY", NULL},
    {"COLOR_BLACK", NULL},
    {"COLOR_BROWN", NULL},
    {"COLOR_ORANGE", NULL},
    {"COLOR_TRANSPARENT", NULL},

    {"CENTER_MODE", NULL},
    {"RIGHT_MODE", NULL},
    {"LEFT_MODE", NULL},

    /* ADDITIONAL CONSTANTS */
    {"LAYERS", NULL},

    {NULL, NULL}};

static void map_constants(lua_State *L)
{
    lua_modules_setfield_integer(L, "COLOR_BLUE", LCD_COLOR_BLUE);
    lua_modules_setfield_integer(L, "COLOR_GREEN", LCD_COLOR_GREEN);
    lua_modules_setfield_integer(L, "COLOR_RED", LCD_COLOR_RED);
    lua_modules_setfield_integer(L, "COLOR_CYAN", LCD_COLOR_CYAN);
    lua_modules_setfield_integer(L, "COLOR_MAGENTA", LCD_COLOR_MAGENTA);
    lua_modules_setfield_integer(L, "COLOR_YELLOW", LCD_COLOR_YELLOW);

    lua_modules_setfield_integer(L, "COLOR_LIGHTBLUE", LCD_COLOR_LIGHTBLUE);
    lua_modules_setfield_integer(L, "COLOR_LIGHTGREEN", LCD_COLOR_LIGHTGREEN);
    lua_modules_setfield_integer(L, "COLOR_LIGHTRED", LCD_COLOR_LIGHTRED);
    lua_modules_setfield_integer(L, "COLOR_LIGHTCYAN", LCD_COLOR_LIGHTCYAN);
    lua_modules_setfield_integer(L, "COLOR_LIGHTMAGENTA", LCD_COLOR_LIGHTMAGENTA);
    lua_modules_setfield_integer(L, "COLOR_LIGHTYELLOW", LCD_COLOR_LIGHTYELLOW);

    lua_modules_setfield_integer(L, "COLOR_DARKBLUE", LCD_COLOR_DARKBLUE);
    lua_modules_setfield_integer(L, "COLOR_DARKGREEN", LCD_COLOR_DARKGREEN);
    lua_modules_setfield_integer(L, "COLOR_DARKRED", LCD_COLOR_DARKRED);
    lua_modules_setfield_integer(L, "COLOR_DARKCYAN", LCD_COLOR_DARKCYAN);
    lua_modules_setfield_integer(L, "COLOR_DARKMAGENTA", LCD_COLOR_DARKMAGENTA);
    lua_modules_setfield_integer(L, "COLOR_DARKYELLOW", LCD_COLOR_DARKYELLOW);

    lua_modules_setfield_integer(L, "COLOR_WHITE", LCD_COLOR_WHITE);
    lua_modules_setfield_integer(L, "COLOR_LIGHTGRAY", LCD_COLOR_LIGHTGRAY);
    lua_modules_setfield_integer(L, "COLOR_GRAY", LCD_COLOR_GRAY);
    lua_modules_setfield_integer(L, "COLOR_DARKGRAY", LCD_COLOR_DARKGRAY);
    lua_modules_setfield_integer(L, "COLOR_BLACK", LCD_COLOR_BLACK);
    lua_modules_setfield_integer(L, "COLOR_BROWN", LCD_COLOR_BROWN);
    lua_modules_setfield_integer(L, "COLOR_ORANGE", LCD_COLOR_ORANGE);
    lua_modules_setfield_integer(L, "COLOR_TRANSPARENT", LCD_COLOR_TRANSPARENT);

    lua_modules_setfield_integer(L, "CENTER_MODE", CENTER_MODE);
    lua_modules_setfield_integer(L, "RIGHT_MODE", RIGHT_MODE);
    lua_modules_setfield_integer(L, "LEFT_MODE", LEFT_MODE);

    lua_modules_setfield_integer(L, "LAYERS", 2);
}

int luaopen_lcd(lua_State *L)
{
    luaL_newlib(L, lib);
    map_constants(L);
    return 1;
}