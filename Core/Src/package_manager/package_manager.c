#include "package_manager/package_manager.h"

#include "FreeRTOS.h"
#include "fatfs.h"
#include "miniz.h"

static char *packages_dir = NULL;
static char *applications_dir = NULL;

static void recursive_mkdir(const char *dir)
{
    const char *prev = dir;
    size_t len = strlen(dir);
    char *tmp = pvPortMalloc(len + 1);

    while (1)
    {
        const char *next = strchr(prev, '/');
        if (next)
        {
            if (next != prev)
            {
                const size_t substr_len = next - dir;
                strncpy(tmp, dir, substr_len);
                tmp[substr_len] = 0;
                f_mkdir(tmp);

                if (next == dir + len - 1)
                {
                    break;
                }
            }
            prev = next + 1;
        }
        else
        {
            f_mkdir(dir);
            break;
        }
    }

    vPortFree(tmp);
}

static void create_dir_if_not_exists(const char *path)
{
    FILINFO file_info;
    FRESULT fr;

    fr = f_stat(path, &file_info);

    if (fr != FR_OK)
    {
        recursive_mkdir(path);
    }
}

FRESULT delete_file_or_dir (
    TCHAR* path,    /* Path name buffer with the sub-directory to delete */
    UINT sz_buff,   /* Size of path name buffer (items) */
    FILINFO* fno    /* Name read buffer */
)
{
    UINT i, j;
    FRESULT fr;
    DIR dir;


    fr = f_opendir(&dir, path); /* Open the directory */
    if (fr != FR_OK) return fr;

    for (i = 0; path[i]; i++) ; /* Get current path length */
    path[i++] = _T('/');

    for (;;) {
        fr = f_readdir(&dir, fno);  /* Get a directory item */
        if (fr != FR_OK || !fno->fname[0]) break;   /* End of directory? */
        j = 0;
        do {    /* Make a path name */
            if (i + j >= sz_buff) { /* Buffer over flow? */
                fr = 100; break;    /* Fails with 100 when buffer overflow */
            }
            path[i + j] = fno->fname[j];
        } while (fno->fname[j++]);
        if (fno->fattrib & AM_DIR) {    /* Item is a directory */
            fr = delete_file_or_dir(path, sz_buff, fno);
        } else {                        /* Item is a file */
            fr = f_unlink(path);
        }
        if (fr != FR_OK) break;
    }

    path[--i] = 0;  /* Restore the path name */
    f_closedir(&dir);

    if (fr == FR_OK) fr = f_unlink(path);  /* Delete the empty directory */
    return fr;
}


ErrorStatus PackageManager_init(const char *new_packages_dir, const char *new_applications_dir)
{
    ErrorStatus res1 = PackageManager_set_packages_dir(new_packages_dir);
    ErrorStatus res2 = PackageManager_set_applications_dir(new_applications_dir);
    return ( (res1 == SUCCESS && res2 == SUCCESS) ? SUCCESS : ERROR);
}

const char *PackageManager_get_packages_dir(void)
{
    return packages_dir;
}

const char * PackageManager_get_applications_dir(void)
{
    return applications_dir;
}

ErrorStatus PackageManager_set_packages_dir(const char *new_packages_dir)
{
    if (!new_packages_dir)
    {
        return ERROR;
    }

    if (packages_dir)
    {
        vPortFree(packages_dir);
        packages_dir = NULL;
    }

    packages_dir = pvPortMalloc(strlen(new_packages_dir) + 1);
    strcpy(packages_dir, new_packages_dir);

    create_dir_if_not_exists(packages_dir);

    return SUCCESS;
}

ErrorStatus PackageManager_set_applications_dir(const char *new_applications_dir)
{
    if (!new_applications_dir)
    {
        return ERROR;
    }

    if (applications_dir)
    {
        vPortFree(applications_dir);
        applications_dir = NULL;
    }

    applications_dir = pvPortMalloc(strlen(new_applications_dir) + 1);
    strcpy(applications_dir, new_applications_dir);

    create_dir_if_not_exists(applications_dir);

    return SUCCESS;
}

ErrorStatus PackageManager_unpack_package(const char *application_name)
{
    mz_zip_archive zip_archive;
    memset(&zip_archive, 0, sizeof(zip_archive));

    char apps_package_path[256];

    if (PackageManager_application_get_package_location(application_name, apps_package_path, sizeof(apps_package_path)) == ERROR)
    {
        return ERROR;
    }

    mz_bool status = mz_zip_reader_init_file(&zip_archive, apps_package_path, 0);

    if (!status)
    {
        return ERROR;
    }

    size_t fileCount = mz_zip_reader_get_num_files(&zip_archive);

    if (fileCount == 0)
    {
        mz_zip_reader_end(&zip_archive);
        return ERROR;
    }

    mz_zip_archive_file_stat file_stat;
    if (!mz_zip_reader_file_stat(&zip_archive, 0, &file_stat)) 
    {
        mz_zip_reader_end(&zip_archive);
        return ERROR;
    }

    const char * apps_dir = PackageManager_get_applications_dir();

    for (size_t i = 0; i < fileCount; ++i)
    {
        if (!mz_zip_reader_file_stat(&zip_archive, i, &file_stat))
        {
            continue;
        }

        const size_t name_length = mz_zip_reader_get_filename(&zip_archive, i, NULL, 0);
        char * name = malloc(name_length);
        char * new_path = malloc(strlen(apps_dir) + sizeof('/') + name_length + 1);

        mz_zip_reader_get_filename(&zip_archive, i, name, name_length);

        sprintf(new_path, "%s/%s", apps_dir, name);

        if (mz_zip_reader_is_file_a_directory(&zip_archive, i))
        {
            new_path[strlen(new_path) - 1] = 0; // delete / at the end, because FatFS does not like it
            FRESULT fr = f_mkdir(new_path);

            if (fr != FR_OK && fr != FR_EXIST)
            {
                free(new_path);
                free(name);
                mz_zip_reader_end(&zip_archive);
                return ERROR;
            }
        }
        else
        {
            if (!mz_zip_reader_extract_to_file(&zip_archive, i, new_path, 0))
            {
                free(new_path);
                free(name);
                mz_zip_reader_end(&zip_archive);
                return ERROR;
            }
        }
        free(new_path);
        free(name);
    }

    mz_zip_reader_end(&zip_archive);

    return SUCCESS;
}

ErrorStatus PackageManager_remove_package(const char *package_name)
{
    FILINFO * fno = pvPortMalloc(sizeof(FILINFO));
    const size_t path_size = strlen(packages_dir) + sizeof('/') + strlen(package_name) + 1;
    char * path = pvPortMalloc(path_size);

    snprintf(path, path_size, "%s/%s", packages_dir, package_name);

    f_unlink(path);

    vPortFree(path);
    vPortFree(fno);

    return SUCCESS;
}

ErrorStatus PackageManager_remove_application(const char *application_name)
{
    FILINFO * fno = pvPortMalloc(sizeof(FILINFO));
    const size_t path_size = strlen(applications_dir) + sizeof('/') + strlen(application_name) + 1;

    const size_t path_buffer_size = 1024;
    char * path = pvPortMalloc(path_buffer_size);

    snprintf(path, path_size, "%s/%s", applications_dir, application_name);

    const ErrorStatus res = delete_file_or_dir(path, path_buffer_size, fno) == FR_OK ? SUCCESS : ERROR;

    vPortFree(fno);
    vPortFree(path);

    return res;
}

int PackageManager_application_is_unpacked(const char *application_name)
{
    const size_t path_size = strlen(applications_dir) + sizeof('/') + strlen(application_name) + 1;
    char * path = pvPortMalloc(path_size);
    FILINFO * fno = pvPortMalloc(sizeof(FILINFO));

    snprintf(path, path_size, "%s/%s", applications_dir, application_name);

    const FRESULT fr = f_stat(path, fno);

    vPortFree(fno);
    vPortFree(path);

    return (fr == FR_OK);
}

ErrorStatus PackageManager_application_get_working_directory(const char *name, char * buffer, size_t buffer_size)
{
    if (!buffer || !buffer_size)
    {
        return ERROR;
    }

    snprintf(buffer, buffer_size, "%s/%s", applications_dir, name);

    return SUCCESS;
}

ErrorStatus PackageManager_application_get_package_location(const char *name, char * buffer, size_t buffer_size)
{
    if (!buffer || !buffer_size)
    {
        return ERROR;
    }

    snprintf(buffer, buffer_size, "%s/%s.zip", packages_dir, name);

    return SUCCESS;
}

ErrorStatus PackageManager_application_get_main_file_path(const char * name, char * buffer, size_t buffer_size)
{
    if (!buffer || !buffer_size)
    {
        return ERROR;
    }

    snprintf(buffer, buffer_size, "%s/%s/main.lua", applications_dir, name);

    return SUCCESS;
}

Array_cstring *PackageManager_get_packages_list(void)
{
    if (!packages_dir)
    {
        return NULL;
    }

    FILINFO  * file_info = pvPortMalloc(sizeof(FILINFO));
    DIR * dir = pvPortMalloc(sizeof(DIR));

    Array_cstring * arr = Array_cstring_new(0);

    FRESULT fr = f_findfirst(dir, file_info, packages_dir, "*.zip");

    while (fr == FR_OK && file_info->fname[0])
    {
        const char str_len = strlen(file_info->fname) + 1;
        char * pch = strrchr(file_info->fname, '.');

        char * package_name = NULL;

        if (pch == NULL)
        {
            package_name = (char *const)calloc(str_len, 1);
            strcpy((char*)package_name, file_info->fname);
        }
        else
        {
            package_name = (char *const)calloc(str_len - 4, 1);
            sscanf(file_info->fname, "%[^.]", package_name);
        }
        
        Array_cstring_append(arr, (cstring*) &package_name);
        fr = f_findnext(dir, file_info);
    }

    f_closedir(dir);

    vPortFree(file_info);
    vPortFree(dir);

    return arr;
}

int directory_contains_file(const char * dir_path, const char * file_name)
{
    FILINFO  * file_info = pvPortMalloc(sizeof(FILINFO));
    char * path = pvPortMalloc(strlen(dir_path) + sizeof('/') + strlen(file_name) + 1);

    sprintf(path, "%s/%s", dir_path, file_name);

    FRESULT fr = f_stat(path, file_info);

    vPortFree(file_info);

    return (fr == FR_OK);
}

Array_cstring *PackageManager_get_applications_list(void)
{
    if (!applications_dir)
    {
        return NULL;
    }

    FILINFO  * file_info = pvPortMalloc(sizeof(FILINFO));
    DIR * dir = pvPortMalloc(sizeof(DIR));
    Array_cstring * arr = Array_cstring_new(0);
    FRESULT res = f_opendir(dir, applications_dir);

    if (res == FR_OK)
    {
        for (;;)
        {
            res = f_readdir(dir, file_info);
            if (res != FR_OK || file_info->fname[0] == 0)
            {
                break;
            }
            else if (file_info->fattrib & AM_DIR)
            {
                char *app_dir_path = pvPortMalloc(strlen(applications_dir) + sizeof('/') + strlen(file_info->fname) + 1);

                sprintf(app_dir_path, "%s/%s", applications_dir, file_info->fname);

                if (directory_contains_file(app_dir_path, "main.lua"))
                {
                    char * app_name = pvPortMalloc(strlen(file_info->fname) + 1);
                    strcpy(app_name, file_info->fname);

                    Array_cstring_append(arr, (cstring*) &app_name);
                }

                vPortFree(app_dir_path);
            }
        }

        f_closedir(dir);
    }

    vPortFree(file_info);
    vPortFree(dir);

    return arr;
}

void PackageManager_free_list(Array_cstring *ptr)
{
    if (!ptr)
    {
        return;
    }

    for (size_t i = 0; i < ptr->size; ++i)
    {
        char * data = NULL;
        if (Array_cstring_get(ptr, i, (cstring*) &data)) {
            vPortFree(data);
        }
    }
    Array_cstring_delete(ptr);
}