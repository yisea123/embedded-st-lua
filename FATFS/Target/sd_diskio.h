/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    sd_diskio.h
  * @brief   Header for sd_diskio.c module
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Note: code generation based on sd_diskio_dma_rtos_template.h */

#ifndef __SD_DISKIO_H
#define __SD_DISKIO_H

#include "stm32f7_diskio_dma_rtos.h"

extern const Diskio_drvTypeDef  SD_Driver;

DSTATUS SD_initialize ();
DSTATUS SD_status ();
DSTATUS SD_disk_status(void);
DRESULT SD_read (BYTE*, DWORD, UINT);
#if FF_USE_WRITE == 1
  DRESULT SD_write (const BYTE*, DWORD, UINT);
#endif /* FF_USE_WRITE == 1 */
#if FF_USE_IOCTL == 1
  DRESULT SD_ioctl (BYTE, void*);
#endif  /* FF_USE_IOCTL == 1 */


#endif /* __SD_DISKIO_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

